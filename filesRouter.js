const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, putFile, delFile } = require('./filesService.js');

router.post('/', createFile);

router.get('/:filename', getFile);

router.get('/', getFiles);

router.put('/:filename', putFile);

router.delete('/:filename', delFile);


module.exports = {
  filesRouter: router
};
