const fs = require('fs');
const path = require('path');
const url = require('url');
// requires...

// constants...

const folderFiles = path.join(__dirname, 'files');
const supportedExtension = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

async function createFile(req, res) {

    // Your code to create the file.
    const fileName = req.body.filename;
    const content = req.body.content;

    if (!fileName) {
        return res.status(400).json({message: "Please specify 'filename' parameter"})
    }
    if (!content) {
        return res.status(400).json({message: "Please specify 'content' parameter"})
    }
    const folderFileName = path.join(folderFiles, fileName)
    const isExtension = supportedExtension.includes(path.extname(fileName));
    if (isExtension) {
        await fs.writeFile(folderFileName, content, (err) => {
            if (err) {
                return res.status(500).json({message: "Server error"});
            }
            res.status(200).json({message: "File created successfully"});
        });
    } else {
        res.status(400).json({message: "Please specify extension file parameter"})
    }
}

async function getFiles(req, res) {
    // Your code to get all files.
    await fs.readdir(folderFiles, (err, files) => {
        if (err) {
            return res.status(500).send({"message": "Server error"});
        }

        res.status(200).send({
            message: "Success",
            files: files
        });
    });
}

const getFile = async (req, res) => {

    const {filename} = req.params;
    const filePath = path.join(folderFiles, filename);
    const isExist = fs.existsSync(filePath);

    if (!isExist) {
        return res.status(400).send({message: `There are no file ${filename} found.`});
    }

    await fs.readFile(filePath, (err, data) => {
        if (err) {
            return res.status(500).send({"message": "Server error"});
        }

        const {birthtime: uploadedDate} = fs.statSync(filePath);
        const response = {
            message: "Success",
            filename,
            content: data.toString(),
            extension: path.extname(filename).replace('.', ''),
            uploadedDate
        };
        res.status(200).send(response);
    });
}

const putFile = async (req, res) => {

    const filePath = path.join(folderFiles, req.params.filename);

    await fs.readFile(filePath, (err) => {

        if (err) {
            return res.status(400).json({message: 'File not found'});
        }

        fs.writeFile(filePath, req.body.content, (err) => {
            if (err) {
                return res.status(500).json({message: 'Server error'});
            }
            res.status(200).json({message: 'File successfully updated!'})
        })
    });
}

const delFile = async (req, res) => {
    const filePath = path.join(folderFiles, req.params.filename);
    await fs.readFile(filePath, (err) => {

        if (err) {
            return res.status(400).json({message: 'File not found'});
        }

        fs.unlink(filePath, (err) => {
            if (err) {
                res.status(500).json({message: 'Server error'});
            }
            res.status(200).json({message: 'File successfully deleted!'})
        })
    });
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    putFile,
    delFile
}